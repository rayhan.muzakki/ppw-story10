from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import *
from selenium import webdriver
from django.contrib.auth.models import User

# Create your tests here.

class trialTest(TestCase):
#---------------- Test URL ---------------------------    
    def test_urlhome_exist(self):
            response= Client().get('/')
            self.assertEqual(response.status_code,200)

    def test_urlregister_exist(self):
            response= Client().get('/signup/')
            self.assertEqual(response.status_code,200)
#-------------------- Test Template ----------------------------
    def test_urlhome_template(self):
        response= Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_urlregister_template(self):
        response= Client().get('/signup/')
        self.assertTemplateUsed(response,'signup.html')
#--------------------- Test Function ---------------------------
    def test_funchome(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_funcsignup(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)    
#------------------------Test Form --------------------------------
    def test_signupuser(self):
        response = Client().post(reverse('home:signup'), data = {'username': 'B1', 'password1' : 'abc124124124', 'password2' : 'abc124124124'})
        count = User.objects.all().count()
        test = User.objects.all().first()
        self.assertEqual(test.username, 'B1')
        self.assertEqual(count,1)

